<?php

/**
 * @file
 * Defines Urbano shipping method and service for Drupal commerce.
 */

/**
 * Implements hook_menu().
 */
function commerce_urbano_menu() {
  $items = array();

  $items['admin/commerce/config/shipping/methods/urbano/edit'] = array(
    'title' => 'Edit',
    'description' => 'Adjust Urbano shipping settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_urbano_settings_form'),
    'access arguments' => array('administer shipping'),
    'file' => 'includes/commerce_urbano.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_INLINE,
    'weight' => 0,
  );

  return $items;
}

/**
 * Implements hook_commerce_shipping_method_info().
 */
function commerce_urbano_commerce_shipping_method_info() {
  $shipping_methods = array();

  $shipping_methods['urbano'] = array(
    'title' => t('Urbano'),
    'description' => t('Urbano shipping services. If you want to hire this service please write to !email. Please note that Urbano will only pick up five ore more packages.', array('!email' => '<a href="mailto:comercial@urbano.com.ar"> comercial@urbano.com.ar</a>')),
  );

  return $shipping_methods;
}

/**
 * Implements hook_commerce_shipping_service_info().
 */
function commerce_urbano_commerce_shipping_service_info() {
  $shipping_services = array();

  $shipping_services['urbano_home_delivery'] = array(
    'title' => t('Home delivery'),
    'description' => t('Direct delivery.'),
    'display_title' => t('Urbano Home delivery'),
    'shipping_method' => 'urbano',
    'price_component' => 'shipping',
    'callbacks' => array(
      'rate' => 'commerce_urbano_service_rate',
    ),
  );

  return $shipping_services;
}

/**
 * Shipping service callback: returns a base price array for a shipping service
 * calculated for the given order.
 */
function commerce_urbano_service_rate($shipping_service, $order) {
  try {
    module_load_include('inc', 'commerce_urbano', 'includes/commerce_urbano.service');

    $urbano = new CommerceUrbanoConnector();
    if ($shipment = $urbano->rateShipping($order)) {
      return array(
        'amount' => $shipment['amount'],
        'currency_code' => $shipment['currency'],
        'data' => array(),
      );
    }
    else {
      return FALSE;
    }
  }
  catch (Exception $e) {
    watchdog_exception('commerce_urbano', $e);

    return FALSE;
  }
}
